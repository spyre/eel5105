--clock de 1hz usado para fazer o contador de 5seg contar em 5 segundos

library ieee;
use ieee.std_logic_1164.all; 
use ieee.std_logic_unsigned.all;

entity clock_1hz is

port(clock_50mhz: in std_logic;
	  RST: in std_logic;
	  clock_1hz: out std_logic
	  );
	  
end clock_1hz;

architecture bhv of clock_1hz is
signal cont: std_logic_vector(27 downto 0);
begin

process (RST, clock_50mhz)
	 
		begin
		if (RST = '0') then
	  		cont <= x"0000000";
			elsif (clock_50mhz'event AND clock_50mhz = '1') then 
				cont <= cont + 1;
				if cont = x"2FAF07F" then
					cont <= x"0000000";
						clock_1hz <= '1';
					else
						clock_1hz <= '0';
				end if;
			end if;
		
end process;
end bhv;