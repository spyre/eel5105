--comparador que mostra quantos acertos a pessoa fez em relaçao ao address da rom escolhido <max 4:>

library ieee;
use ieee.std_logic_1164.all;
--use IEEE.std_logic_arith.all;
use IEEE.std_logic_unsigned.all;

entity comparador_8bits is 

port(reg_switches: in std_logic_vector(9 downto 0);
	  saida_rom: in std_logic_vector(9 downto 0);
	  numero_de_acertos: out std_logic_vector(2 downto 0)
);
end comparador_8bits;

architecture bhv of comparador_8bits is
signal saida_comparador0, saida_comparador1, saida_comparador2, saida_comparador3, saida_comparador4, saida_comparador5, saida_comparador6, saida_comparador7, saida_comparador8, saida_comparador9: std_logic;
begin

saida_comparador0 <= (reg_switches(0) AND saida_rom(0));

saida_comparador1 <= (reg_switches(1) AND saida_rom(1));

saida_comparador2 <= (reg_switches(2) AND saida_rom(2));

saida_comparador3 <= (reg_switches(3) AND saida_rom(3));

saida_comparador4 <= (reg_switches(4) AND saida_rom(4));

saida_comparador5 <= (reg_switches(5) AND saida_rom(5));

saida_comparador6 <= (reg_switches(6) AND saida_rom(6));

saida_comparador7 <= (reg_switches(7) AND saida_rom(7));

saida_comparador8 <= (reg_switches(8) AND saida_rom(8));

saida_comparador9 <= (reg_switches(9) AND saida_rom(9));

numero_de_acertos <= ("00" & saida_comparador9) + ("00" & saida_comparador8) + ("00" & saida_comparador7) + ("00" & saida_comparador6) + ("00" & saida_comparador5) + ("00" & saida_comparador4) + ("00" & saida_comparador3) + ("00" & saida_comparador2) + ("00" & saida_comparador1) + ("00" & saida_comparador0);

end bhv;

