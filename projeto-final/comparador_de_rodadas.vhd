--esse componente calcula C, que um bool indicando se a pessoa acertou ou nao a sequencia de combinacoes

library ieee;
use ieee.std_logic_1164.all;

entity comparador_de_rodadas is 
port(saida_reg4: in std_logic_vector(3 downto 0);
	 saida_comparador_de_rodadas: out std_logic
	 );
	 
end comparador_de_rodadas;

architecture bhv of comparador_de_rodadas is 
begin

saida_comparador_de_rodadas <= '1' when saida_reg4 = "1010" else '0'; 

end bhv;