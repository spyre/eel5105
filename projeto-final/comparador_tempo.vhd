--esse componente da um sinal quando o tempo de uma rodada de um user tiver acabado (em bool)

library ieee;
use ieee.std_logic_1164.all;

entity comparador_tempo is 
port(saida_reg3: in std_logic_vector(2 downto 0);
	 saida_comparador_tempo: out std_logic
	 );
	 
end comparador_tempo;

architecture bhv of comparador_tempo is 
begin

saida_comparador_tempo <= '1' when saida_reg3 = "0101" else '0'; 

end bhv;