--contador de 5seg usado para o timer

library ieee;
use ieee.std_logic_1164.all; 
use ieee.std_logic_unsigned.all;

entity contador is
port(CLOCK: in std_logic;
	  COUNT_TO_FIVE: out std_logic_vector(2 downto 0);
	ENABLE: in std_logic
	  );
	  
end contador;

architecture bhv of contador is
signal COUNT: std_logic_vector(2 downto 0);
begin

	process(clock, enable)
	begin
		if(ENABLE = '1') then
			elsif(CLOCK'event and CLOCK = '1') then
				COUNT <= COUNT + "001";
				COUNT_TO_FIVE <= COUNT;
				if (COUNT = "101") then
					COUNT <= "000";					
				end if;
			end if;
		end process;
end bhv;

