--contador de 10 usado para manter tracking de quantas rodadas ja foram jogadas

library ieee;
use ieee.std_logic_1164.all; 
--use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;

entity contador_10 is

port(CONTAR: in std_logic;
	 SAIDA_REG4: in std_logic_vector(3 downto 0);
	 SAIDA: out std_logic_vector(3 downto 0)
	 );
	  
end contador_10;

architecture bhv of contador_10 is

begin
--Quando o botao for pressionado, a saida recebe 1 mais o valor ja guardado na memoria
SAIDA <= std_logic_vector("0001" + unsigned(SAIDA_REG4)) when CONTAR = '0';

end bhv;