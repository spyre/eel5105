--contador de 5seg usado para o timer

library ieee;
use ieee.std_logic_1164.all; 
use ieee.std_logic_unsigned.all;
--use ieee.numeric_std.all;

entity contador_5 is
port(CLOCK: in std_logic;
	 ENABLE: in std_logic;
	 COUNT_SAIDA: out std_logic_vector(2 downto 0)
	 );
	  
end contador_5;

architecture bhv of contador_5 is
signal COUNT_TO_FIVE: std_logic_vector(2 downto 0);
begin

	process(CLOCK, ENABLE)
	begin
		if(ENABLE = '1') then
			if(CLOCK'event and CLOCK = '1') then
				COUNT_TO_FIVE <= COUNT_TO_FIVE + 1;			
				COUNT_SAIDA <= COUNT_TO_FIVE;
				if(COUNT_TO_FIVE = "101") then
					COUNT_TO_FIVE <= "000";
				end if;
			end if;
		end if;
	end process;
end bhv;

