--esse componente funciona dando outputs diferentes para cada combinacao igual de 0-25, magicamente mostrando os numeros em decimal no display

library ieee;
use ieee.std_logic_1164.all; 

entity decodificador_resultado_final is
port(saida_do_resultado_final: in std_logic_vector(4 downto 0);
	 HEX0: out std_logic_vector(6 downto 0);
	 HEX1: out std_logic_vector(6 downto 0)
	 );
	 
end decodificador_resultado_final;

architecture bhv of decodificador_resultado_final is

begin
--primeiro HEX: digito a esquerda
HEX0 <= "1000000" when saida_do_resultado_final = "00000" else --0
		"1111001" when saida_do_resultado_final = "00001" else --1
		"0100100" when saida_do_resultado_final = "00010" else --2
		"0110000" when saida_do_resultado_final = "00011" else --3
		"0011001" when saida_do_resultado_final = "00100" else --4
		"0010010" when saida_do_resultado_final = "00101" else --5
		"0000010" when saida_do_resultado_final = "00110" else --6 
		"1111000" when saida_do_resultado_final = "00111" else --7
		"0000000" when saida_do_resultado_final = "01000" else --8
		"0010000" when saida_do_resultado_final = "01001" else --9
		"1000000" when saida_do_resultado_final = "01010" else --10
		"1111001" when saida_do_resultado_final = "01011" else --11
		"0100100" when saida_do_resultado_final = "01100" else --12
		"0110000" when saida_do_resultado_final = "01101" else --13
		"0011001" when saida_do_resultado_final = "01110" else --14
		"0010010" when saida_do_resultado_final = "01111" else --15
		"0000010" when saida_do_resultado_final = "10000" else --16
		"1111000" when saida_do_resultado_final = "10001" else --17
		"0000000" when saida_do_resultado_final = "10010" else --18
		"0010000" when saida_do_resultado_final = "10011" else --19
		"0000000" when saida_do_resultado_final = "10100" else --20
		"1111001" when saida_do_resultado_final = "10101" else --21
		"0100100" when saida_do_resultado_final = "10110" else --22
		"0110000" when saida_do_resultado_final = "10111" else --23
		"0011001" when saida_do_resultado_final = "11000" else --24
		"0010010";

--segundo HEX: digito a direita		

HEX1 <= "0000000" when saida_do_resultado_final = "00000" else --0
		"0000000" when saida_do_resultado_final = "00001" else --1
		"0000000" when saida_do_resultado_final = "00010" else --2
		"0000000" when saida_do_resultado_final = "00011" else --3
		"0000000" when saida_do_resultado_final = "00100" else --4
		"0000000" when saida_do_resultado_final = "00101" else --5
		"0000000" when saida_do_resultado_final = "00110" else --6 
		"0000000" when saida_do_resultado_final = "00111" else --7
		"0000000" when saida_do_resultado_final = "01000" else --8
		"0000000" when saida_do_resultado_final = "01001" else --9
		"1111001" when saida_do_resultado_final = "01010" else --10
		"1111001" when saida_do_resultado_final = "01011" else --11
		"1111001" when saida_do_resultado_final = "01100" else --12
		"1111001" when saida_do_resultado_final = "01101" else --13
		"1111001" when saida_do_resultado_final = "01110" else --14
		"1111001" when saida_do_resultado_final = "01111" else --15
		"1111001" when saida_do_resultado_final = "10000" else --16
		"1111001" when saida_do_resultado_final = "10001" else --17
		"1111001" when saida_do_resultado_final = "10010" else --18
		"1111001" when saida_do_resultado_final = "10011" else --19
		"0100100" when saida_do_resultado_final = "10100" else --20
		"0100100" when saida_do_resultado_final = "10101" else --21
		"0100100" when saida_do_resultado_final = "10110" else --22
		"0100100" when saida_do_resultado_final = "10111" else --23
		"0100100" when saida_do_resultado_final = "11000" else --24
		"0100100";
		
end bhv;