 library ieee;
use ieee.std_logic_1164.all; 

entity decodificador_rodada is
port(saida_reg3: in std_logic_vector(3 downto 0);
	 HEX1: out std_logic_vector(6 downto 0);
	 HEX2: out std_logic_vector(6 downto 0)
	 );
	 
end decodificador_rodada;

architecture bhv of decodificador_rodada is

begin

HEX1 <= "1000000" when saida_reg3 = "0000" else --0
		"1111001" when saida_reg3 = "0001" else --1
		"0100100" when saida_reg3 = "0010" else --2
		"0110000" when saida_reg3 = "0011" else --3
		"0011001" when saida_reg3 = "0100" else --4
		"0010010" when saida_reg3 = "0101" else --5
		"0000010" when saida_reg3 = "0110" else --6 
		"1111000" when saida_reg3 = "0111" else --7
		"0010000" when saida_reg3 = "1000" else --8
		"1000000";

HEX2 <= "1001110" when saida_reg3 = "0000" else --0
		"1001110" when saida_reg3 = "0001" else --1
		"1001110" when saida_reg3 = "0010" else --2
		"1001110" when saida_reg3 = "0011" else --3
		"1001110" when saida_reg3 = "0100" else --4
		"1001110" when saida_reg3 = "0101" else --5
		"1001110" when saida_reg3 = "0110" else --6 
		"1001110" when saida_reg3 = "0111" else --7
		"1001110" when saida_reg3 = "1000" else --8
		"1001110";
end bhv;