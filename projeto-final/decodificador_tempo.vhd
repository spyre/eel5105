library ieee;
use ieee.std_logic_1164.all; 

entity decodificador_tempo is
port(saida_reg3: in std_logic_vector(2 downto 0);
	 HEX1: out std_logic_vector(6 downto 0);
	 HEX2: out std_logic_vector(6 downto 0)
	);
	 
end decodificador_tempo;

architecture bhv of decodificador_tempo is

begin

HEX1 <= "1000000" when saida_reg3 = "000" else --0
		"1111001" when saida_reg3 = "001" else --1
		"0100100" when saida_reg3 = "010" else --2
		"0110000" when saida_reg3 = "011" else --3
		"0011001" when saida_reg3 = "100" else --4
		"0010010" when saida_reg3 = "101" else --5
		"0000010" when saida_reg3 = "110" else --6 
		"1111000";
		
HEX2 <= "0000111" when saida_reg3 = "000" else --0
		"0000111" when saida_reg3 = "001" else --1
		"0000111" when saida_reg3 = "010" else --2
		"0000111" when saida_reg3 = "011" else --3
		"0000111" when saida_reg3 = "100" else --4
		"0000111" when saida_reg3 = "101" else --5
		"0000111" when saida_reg3 = "110" else --6 
		"0000111";
end bhv;