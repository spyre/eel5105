--registrador usado para guardar a memoria escolhida

library ieee;
use ieee.std_logic_1164.all;

entity reg10 is port (
CLK: in std_logic;
EN: in std_logic;
RST: in std_logic;
D: in std_logic_vector(9 downto 0);
Q: out std_logic_vector(9 downto 0));

end reg10;

architecture behv of reg10 is
begin
	process(CLK)
	begin
	if (RST = '1') then
		Q <= "00000000";
		elsif (CLK'event and CLK = '1') then 
			if (EN = '0') then -- se transicao de clock e enable 0 (alto baixo), q vai pra d
			Q <= D;
		end if;
	end if;
	end process;

end behv;
