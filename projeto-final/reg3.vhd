--registrador usado para guardar o valor do contador de 5seg

library ieee;
use ieee.std_logic_1164.all;

entity reg3 is port (
CLK: in std_logic;
EN: in std_logic;
RST: in std_logic;
D: in std_logic_vector(2 downto 0);
Q: out std_logic_vector(2 downto 0));

end reg3;

architecture behv of reg3 is
begin
	process(CLK)
	begin
	if (RST = '1') then
		Q <= "00000000";
		elsif (CLK'event and CLK = '1') then 
			if (EN = '0') then -- se transicao de clock e enable 0 (alto baixo), q vai pra d
			Q <= D;
		end if;
	end if;
	end process;

end behv;