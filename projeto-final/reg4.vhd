--registrador usado para guardar o numero de rodadas ja jogadas

library ieee;
use ieee.std_logic_1164.all;

entity reg4 is port (
CLK: in std_logic;
EN: in std_logic;
RST: in std_logic;
D: in std_logic_vector(3 downto 0);
Q: out std_logic_vector(3 downto 0));

end reg4;

architecture behv of reg4 is
begin
	process(CLK)
	begin
	if (RST = '1') then
		Q <= "0000";
		elsif (CLK'event and CLK = '1') then 
			if (EN = '0') then -- se transicao de clock e enable 0 (alto baixo), q vai pra d
			Q <= D;
		end if;
	end if;
	end process;

end behv;