--entradas desse componente: saida_acerto_comparador sera a saida de comparador_de_rodadas e saida_contador_rodadas sera a saida do registrador de 4 bits

library ieee;
use ieee.std_logic_1164.all;
--use IEEE.std_logic_arith.all;
--use IEEE.std_logic_unsigned.all;
use ieee.numeric_std.all;

entity resultado_final is
port(saida_acerto_comparador: in std_logic; -- C
	 saida_contador_rodadas: in std_logic_vector(3 downto 0); -- N saida registrador do counter de 10
	 resultado_final: out std_logic_vector(4 downto 0)
	  );
	  
end resultado_final;

architecture bhv of resultado_final is
begin

resultado_final <= std_logic_vector("10000" + ("01010" - unsigned('0'&(saida_contador_rodadas)))) when saida_acerto_comparador = '1' else
std_logic_vector("01010" - unsigned('0'&(saida_contador_rodadas))); 

end bhv;
