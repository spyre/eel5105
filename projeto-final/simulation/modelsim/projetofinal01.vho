-- Copyright (C) 1991-2015 Altera Corporation. All rights reserved.
-- Your use of Altera Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Altera Program License 
-- Subscription Agreement, the Altera Quartus Prime License Agreement,
-- the Altera MegaCore Function License Agreement, or other 
-- applicable license agreement, including, without limitation, 
-- that your use is for the sole purpose of programming logic 
-- devices manufactured by Altera and sold by Altera or its 
-- authorized distributors.  Please refer to the applicable 
-- agreement for further details.

-- VENDOR "Altera"
-- PROGRAM "Quartus Prime"
-- VERSION "Version 15.1.0 Build 185 10/21/2015 SJ Lite Edition"

-- DATE "06/23/2018 15:29:16"

-- 
-- Device: Altera 5CSEMA5F31C6 Package FBGA896
-- 

-- 
-- This VHDL file should be used for ModelSim-Altera (VHDL) only
-- 

LIBRARY ALTERA_LNSIM;
LIBRARY CYCLONEV;
LIBRARY IEEE;
USE ALTERA_LNSIM.ALTERA_LNSIM_COMPONENTS.ALL;
USE CYCLONEV.CYCLONEV_COMPONENTS.ALL;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY 	comparador_8bits IS
    PORT (
	reg_switches : IN std_logic_vector(9 DOWNTO 0);
	saida_rom : IN std_logic_vector(9 DOWNTO 0);
	numero_de_acertos : OUT std_logic_vector(2 DOWNTO 0)
	);
END comparador_8bits;

-- Design Ports Information
-- numero_de_acertos[0]	=>  Location: PIN_AE16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- numero_de_acertos[1]	=>  Location: PIN_AJ21,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- numero_de_acertos[2]	=>  Location: PIN_AJ16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- reg_switches[2]	=>  Location: PIN_AJ19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- saida_rom[2]	=>  Location: PIN_AH19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- reg_switches[7]	=>  Location: PIN_W16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- saida_rom[7]	=>  Location: PIN_AK18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- reg_switches[8]	=>  Location: PIN_AF19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- saida_rom[8]	=>  Location: PIN_AK19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- reg_switches[9]	=>  Location: PIN_AG21,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- saida_rom[9]	=>  Location: PIN_AJ17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- reg_switches[5]	=>  Location: PIN_AG18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- saida_rom[5]	=>  Location: PIN_AH20,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- reg_switches[6]	=>  Location: PIN_AA16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- saida_rom[6]	=>  Location: PIN_W17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- reg_switches[3]	=>  Location: PIN_AK16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- saida_rom[3]	=>  Location: PIN_AH17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- reg_switches[4]	=>  Location: PIN_AF16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- saida_rom[4]	=>  Location: PIN_V17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- reg_switches[1]	=>  Location: PIN_AB17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- saida_rom[1]	=>  Location: PIN_AJ20,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- saida_rom[0]	=>  Location: PIN_V16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- reg_switches[0]	=>  Location: PIN_AH18,	 I/O Standard: 2.5 V,	 Current Strength: Default


ARCHITECTURE structure OF comparador_8bits IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL unknown : std_logic := 'X';
SIGNAL devoe : std_logic := '1';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL ww_reg_switches : std_logic_vector(9 DOWNTO 0);
SIGNAL ww_saida_rom : std_logic_vector(9 DOWNTO 0);
SIGNAL ww_numero_de_acertos : std_logic_vector(2 DOWNTO 0);
SIGNAL \~QUARTUS_CREATED_GND~I_combout\ : std_logic;
SIGNAL \reg_switches[7]~input_o\ : std_logic;
SIGNAL \saida_rom[7]~input_o\ : std_logic;
SIGNAL \saida_comparador7~combout\ : std_logic;
SIGNAL \saida_rom[8]~input_o\ : std_logic;
SIGNAL \saida_rom[9]~input_o\ : std_logic;
SIGNAL \reg_switches[9]~input_o\ : std_logic;
SIGNAL \reg_switches[8]~input_o\ : std_logic;
SIGNAL \Add0~0_combout\ : std_logic;
SIGNAL \saida_rom[2]~input_o\ : std_logic;
SIGNAL \reg_switches[2]~input_o\ : std_logic;
SIGNAL \saida_comparador2~combout\ : std_logic;
SIGNAL \reg_switches[1]~input_o\ : std_logic;
SIGNAL \saida_rom[1]~input_o\ : std_logic;
SIGNAL \saida_comparador1~combout\ : std_logic;
SIGNAL \saida_rom[3]~input_o\ : std_logic;
SIGNAL \saida_rom[4]~input_o\ : std_logic;
SIGNAL \reg_switches[3]~input_o\ : std_logic;
SIGNAL \reg_switches[4]~input_o\ : std_logic;
SIGNAL \reg_switches[6]~input_o\ : std_logic;
SIGNAL \saida_rom[6]~input_o\ : std_logic;
SIGNAL \saida_comparador6~combout\ : std_logic;
SIGNAL \saida_rom[5]~input_o\ : std_logic;
SIGNAL \reg_switches[5]~input_o\ : std_logic;
SIGNAL \saida_comparador5~combout\ : std_logic;
SIGNAL \Add3~12_combout\ : std_logic;
SIGNAL \saida_rom[0]~input_o\ : std_logic;
SIGNAL \reg_switches[0]~input_o\ : std_logic;
SIGNAL \Add3~15_cout\ : std_logic;
SIGNAL \Add3~1_sumout\ : std_logic;
SIGNAL \Add1~0_combout\ : std_logic;
SIGNAL \Add7~0_combout\ : std_logic;
SIGNAL \Add3~17_combout\ : std_logic;
SIGNAL \Add3~2\ : std_logic;
SIGNAL \Add3~5_sumout\ : std_logic;
SIGNAL \Add3~18_combout\ : std_logic;
SIGNAL \Add7~1_combout\ : std_logic;
SIGNAL \Add3~6\ : std_logic;
SIGNAL \Add3~9_sumout\ : std_logic;
SIGNAL \ALT_INV_reg_switches[0]~input_o\ : std_logic;
SIGNAL \ALT_INV_saida_rom[0]~input_o\ : std_logic;
SIGNAL \ALT_INV_saida_rom[1]~input_o\ : std_logic;
SIGNAL \ALT_INV_reg_switches[1]~input_o\ : std_logic;
SIGNAL \ALT_INV_saida_rom[4]~input_o\ : std_logic;
SIGNAL \ALT_INV_reg_switches[4]~input_o\ : std_logic;
SIGNAL \ALT_INV_saida_rom[3]~input_o\ : std_logic;
SIGNAL \ALT_INV_reg_switches[3]~input_o\ : std_logic;
SIGNAL \ALT_INV_saida_rom[6]~input_o\ : std_logic;
SIGNAL \ALT_INV_reg_switches[6]~input_o\ : std_logic;
SIGNAL \ALT_INV_saida_rom[5]~input_o\ : std_logic;
SIGNAL \ALT_INV_reg_switches[5]~input_o\ : std_logic;
SIGNAL \ALT_INV_saida_rom[9]~input_o\ : std_logic;
SIGNAL \ALT_INV_reg_switches[9]~input_o\ : std_logic;
SIGNAL \ALT_INV_saida_rom[8]~input_o\ : std_logic;
SIGNAL \ALT_INV_reg_switches[8]~input_o\ : std_logic;
SIGNAL \ALT_INV_saida_rom[7]~input_o\ : std_logic;
SIGNAL \ALT_INV_reg_switches[7]~input_o\ : std_logic;
SIGNAL \ALT_INV_saida_rom[2]~input_o\ : std_logic;
SIGNAL \ALT_INV_reg_switches[2]~input_o\ : std_logic;
SIGNAL \ALT_INV_Add7~1_combout\ : std_logic;
SIGNAL \ALT_INV_Add3~18_combout\ : std_logic;
SIGNAL \ALT_INV_Add7~0_combout\ : std_logic;
SIGNAL \ALT_INV_Add1~0_combout\ : std_logic;
SIGNAL \ALT_INV_Add3~17_combout\ : std_logic;
SIGNAL \ALT_INV_saida_comparador1~combout\ : std_logic;
SIGNAL \ALT_INV_Add3~12_combout\ : std_logic;
SIGNAL \ALT_INV_saida_comparador6~combout\ : std_logic;
SIGNAL \ALT_INV_saida_comparador5~combout\ : std_logic;
SIGNAL \ALT_INV_Add0~0_combout\ : std_logic;
SIGNAL \ALT_INV_saida_comparador7~combout\ : std_logic;
SIGNAL \ALT_INV_saida_comparador2~combout\ : std_logic;

BEGIN

ww_reg_switches <= reg_switches;
ww_saida_rom <= saida_rom;
numero_de_acertos <= ww_numero_de_acertos;
ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;
\ALT_INV_reg_switches[0]~input_o\ <= NOT \reg_switches[0]~input_o\;
\ALT_INV_saida_rom[0]~input_o\ <= NOT \saida_rom[0]~input_o\;
\ALT_INV_saida_rom[1]~input_o\ <= NOT \saida_rom[1]~input_o\;
\ALT_INV_reg_switches[1]~input_o\ <= NOT \reg_switches[1]~input_o\;
\ALT_INV_saida_rom[4]~input_o\ <= NOT \saida_rom[4]~input_o\;
\ALT_INV_reg_switches[4]~input_o\ <= NOT \reg_switches[4]~input_o\;
\ALT_INV_saida_rom[3]~input_o\ <= NOT \saida_rom[3]~input_o\;
\ALT_INV_reg_switches[3]~input_o\ <= NOT \reg_switches[3]~input_o\;
\ALT_INV_saida_rom[6]~input_o\ <= NOT \saida_rom[6]~input_o\;
\ALT_INV_reg_switches[6]~input_o\ <= NOT \reg_switches[6]~input_o\;
\ALT_INV_saida_rom[5]~input_o\ <= NOT \saida_rom[5]~input_o\;
\ALT_INV_reg_switches[5]~input_o\ <= NOT \reg_switches[5]~input_o\;
\ALT_INV_saida_rom[9]~input_o\ <= NOT \saida_rom[9]~input_o\;
\ALT_INV_reg_switches[9]~input_o\ <= NOT \reg_switches[9]~input_o\;
\ALT_INV_saida_rom[8]~input_o\ <= NOT \saida_rom[8]~input_o\;
\ALT_INV_reg_switches[8]~input_o\ <= NOT \reg_switches[8]~input_o\;
\ALT_INV_saida_rom[7]~input_o\ <= NOT \saida_rom[7]~input_o\;
\ALT_INV_reg_switches[7]~input_o\ <= NOT \reg_switches[7]~input_o\;
\ALT_INV_saida_rom[2]~input_o\ <= NOT \saida_rom[2]~input_o\;
\ALT_INV_reg_switches[2]~input_o\ <= NOT \reg_switches[2]~input_o\;
\ALT_INV_Add7~1_combout\ <= NOT \Add7~1_combout\;
\ALT_INV_Add3~18_combout\ <= NOT \Add3~18_combout\;
\ALT_INV_Add7~0_combout\ <= NOT \Add7~0_combout\;
\ALT_INV_Add1~0_combout\ <= NOT \Add1~0_combout\;
\ALT_INV_Add3~17_combout\ <= NOT \Add3~17_combout\;
\ALT_INV_saida_comparador1~combout\ <= NOT \saida_comparador1~combout\;
\ALT_INV_Add3~12_combout\ <= NOT \Add3~12_combout\;
\ALT_INV_saida_comparador6~combout\ <= NOT \saida_comparador6~combout\;
\ALT_INV_saida_comparador5~combout\ <= NOT \saida_comparador5~combout\;
\ALT_INV_Add0~0_combout\ <= NOT \Add0~0_combout\;
\ALT_INV_saida_comparador7~combout\ <= NOT \saida_comparador7~combout\;
\ALT_INV_saida_comparador2~combout\ <= NOT \saida_comparador2~combout\;

-- Location: IOOBUF_X52_Y0_N36
\numero_de_acertos[0]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \Add3~1_sumout\,
	devoe => ww_devoe,
	o => ww_numero_de_acertos(0));

-- Location: IOOBUF_X62_Y0_N53
\numero_de_acertos[1]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \Add3~5_sumout\,
	devoe => ww_devoe,
	o => ww_numero_de_acertos(1));

-- Location: IOOBUF_X54_Y0_N36
\numero_de_acertos[2]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \Add3~9_sumout\,
	devoe => ww_devoe,
	o => ww_numero_de_acertos(2));

-- Location: IOIBUF_X52_Y0_N18
\reg_switches[7]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_reg_switches(7),
	o => \reg_switches[7]~input_o\);

-- Location: IOIBUF_X58_Y0_N58
\saida_rom[7]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_saida_rom(7),
	o => \saida_rom[7]~input_o\);

-- Location: LABCELL_X56_Y2_N51
saida_comparador7 : cyclonev_lcell_comb
-- Equation(s):
-- \saida_comparador7~combout\ = ( \saida_rom[7]~input_o\ & ( \reg_switches[7]~input_o\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000001010101010101010101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_reg_switches[7]~input_o\,
	dataf => \ALT_INV_saida_rom[7]~input_o\,
	combout => \saida_comparador7~combout\);

-- Location: IOIBUF_X60_Y0_N52
\saida_rom[8]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_saida_rom(8),
	o => \saida_rom[8]~input_o\);

-- Location: IOIBUF_X58_Y0_N41
\saida_rom[9]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_saida_rom(9),
	o => \saida_rom[9]~input_o\);

-- Location: IOIBUF_X54_Y0_N1
\reg_switches[9]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_reg_switches(9),
	o => \reg_switches[9]~input_o\);

-- Location: IOIBUF_X62_Y0_N1
\reg_switches[8]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_reg_switches(8),
	o => \reg_switches[8]~input_o\);

-- Location: LABCELL_X56_Y2_N12
\Add0~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add0~0_combout\ = (!\saida_rom[8]~input_o\ & (\saida_rom[9]~input_o\ & (\reg_switches[9]~input_o\))) # (\saida_rom[8]~input_o\ & (!\reg_switches[8]~input_o\ $ (((!\saida_rom[9]~input_o\) # (!\reg_switches[9]~input_o\)))))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000001101010110000000110101011000000011010101100000001101010110",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_saida_rom[8]~input_o\,
	datab => \ALT_INV_saida_rom[9]~input_o\,
	datac => \ALT_INV_reg_switches[9]~input_o\,
	datad => \ALT_INV_reg_switches[8]~input_o\,
	combout => \Add0~0_combout\);

-- Location: IOIBUF_X58_Y0_N92
\saida_rom[2]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_saida_rom(2),
	o => \saida_rom[2]~input_o\);

-- Location: IOIBUF_X60_Y0_N35
\reg_switches[2]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_reg_switches(2),
	o => \reg_switches[2]~input_o\);

-- Location: LABCELL_X56_Y2_N15
saida_comparador2 : cyclonev_lcell_comb
-- Equation(s):
-- \saida_comparador2~combout\ = ( \reg_switches[2]~input_o\ & ( \saida_rom[2]~input_o\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000001111000011110000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \ALT_INV_saida_rom[2]~input_o\,
	dataf => \ALT_INV_reg_switches[2]~input_o\,
	combout => \saida_comparador2~combout\);

-- Location: IOIBUF_X56_Y0_N18
\reg_switches[1]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_reg_switches(1),
	o => \reg_switches[1]~input_o\);

-- Location: IOIBUF_X62_Y0_N35
\saida_rom[1]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_saida_rom(1),
	o => \saida_rom[1]~input_o\);

-- Location: LABCELL_X56_Y2_N48
saida_comparador1 : cyclonev_lcell_comb
-- Equation(s):
-- \saida_comparador1~combout\ = ( \saida_rom[1]~input_o\ & ( \reg_switches[1]~input_o\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000001111000011110000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \ALT_INV_reg_switches[1]~input_o\,
	dataf => \ALT_INV_saida_rom[1]~input_o\,
	combout => \saida_comparador1~combout\);

-- Location: IOIBUF_X56_Y0_N35
\saida_rom[3]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_saida_rom(3),
	o => \saida_rom[3]~input_o\);

-- Location: IOIBUF_X60_Y0_N1
\saida_rom[4]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_saida_rom(4),
	o => \saida_rom[4]~input_o\);

-- Location: IOIBUF_X54_Y0_N52
\reg_switches[3]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_reg_switches(3),
	o => \reg_switches[3]~input_o\);

-- Location: IOIBUF_X52_Y0_N52
\reg_switches[4]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_reg_switches(4),
	o => \reg_switches[4]~input_o\);

-- Location: IOIBUF_X56_Y0_N1
\reg_switches[6]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_reg_switches(6),
	o => \reg_switches[6]~input_o\);

-- Location: IOIBUF_X60_Y0_N18
\saida_rom[6]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_saida_rom(6),
	o => \saida_rom[6]~input_o\);

-- Location: LABCELL_X56_Y2_N27
saida_comparador6 : cyclonev_lcell_comb
-- Equation(s):
-- \saida_comparador6~combout\ = ( \saida_rom[6]~input_o\ & ( \reg_switches[6]~input_o\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000001010101010101010101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_reg_switches[6]~input_o\,
	dataf => \ALT_INV_saida_rom[6]~input_o\,
	combout => \saida_comparador6~combout\);

-- Location: IOIBUF_X54_Y0_N18
\saida_rom[5]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_saida_rom(5),
	o => \saida_rom[5]~input_o\);

-- Location: IOIBUF_X58_Y0_N75
\reg_switches[5]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_reg_switches(5),
	o => \reg_switches[5]~input_o\);

-- Location: LABCELL_X56_Y2_N24
saida_comparador5 : cyclonev_lcell_comb
-- Equation(s):
-- \saida_comparador5~combout\ = ( \reg_switches[5]~input_o\ & ( \saida_rom[5]~input_o\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000110011001100110011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_saida_rom[5]~input_o\,
	dataf => \ALT_INV_reg_switches[5]~input_o\,
	combout => \saida_comparador5~combout\);

-- Location: LABCELL_X56_Y2_N30
\Add3~12\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add3~12_combout\ = ( \saida_comparador6~combout\ & ( \saida_comparador5~combout\ & ( (!\saida_rom[3]~input_o\ & (\saida_rom[4]~input_o\ & ((\reg_switches[4]~input_o\)))) # (\saida_rom[3]~input_o\ & (!\reg_switches[3]~input_o\ $ 
-- (((!\saida_rom[4]~input_o\) # (!\reg_switches[4]~input_o\))))) ) ) ) # ( !\saida_comparador6~combout\ & ( \saida_comparador5~combout\ & ( (!\saida_rom[3]~input_o\ & ((!\saida_rom[4]~input_o\) # ((!\reg_switches[4]~input_o\)))) # (\saida_rom[3]~input_o\ & 
-- (!\reg_switches[3]~input_o\ $ (((\saida_rom[4]~input_o\ & \reg_switches[4]~input_o\))))) ) ) ) # ( \saida_comparador6~combout\ & ( !\saida_comparador5~combout\ & ( (!\saida_rom[3]~input_o\ & ((!\saida_rom[4]~input_o\) # ((!\reg_switches[4]~input_o\)))) # 
-- (\saida_rom[3]~input_o\ & (!\reg_switches[3]~input_o\ $ (((\saida_rom[4]~input_o\ & \reg_switches[4]~input_o\))))) ) ) ) # ( !\saida_comparador6~combout\ & ( !\saida_comparador5~combout\ & ( (!\saida_rom[3]~input_o\ & (\saida_rom[4]~input_o\ & 
-- ((\reg_switches[4]~input_o\)))) # (\saida_rom[3]~input_o\ & (!\reg_switches[3]~input_o\ $ (((!\saida_rom[4]~input_o\) # (!\reg_switches[4]~input_o\))))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000010100110110111110101100100111111010110010010000010100110110",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_saida_rom[3]~input_o\,
	datab => \ALT_INV_saida_rom[4]~input_o\,
	datac => \ALT_INV_reg_switches[3]~input_o\,
	datad => \ALT_INV_reg_switches[4]~input_o\,
	datae => \ALT_INV_saida_comparador6~combout\,
	dataf => \ALT_INV_saida_comparador5~combout\,
	combout => \Add3~12_combout\);

-- Location: IOIBUF_X52_Y0_N1
\saida_rom[0]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_saida_rom(0),
	o => \saida_rom[0]~input_o\);

-- Location: IOIBUF_X56_Y0_N52
\reg_switches[0]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_reg_switches(0),
	o => \reg_switches[0]~input_o\);

-- Location: LABCELL_X56_Y2_N0
\Add3~15\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add3~15_cout\ = CARRY(( \reg_switches[0]~input_o\ ) + ( \saida_rom[0]~input_o\ ) + ( !VCC ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111100001111000000000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \ALT_INV_saida_rom[0]~input_o\,
	datad => \ALT_INV_reg_switches[0]~input_o\,
	cin => GND,
	cout => \Add3~15_cout\);

-- Location: LABCELL_X56_Y2_N3
\Add3~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add3~1_sumout\ = SUM(( !\saida_comparador7~combout\ $ (!\Add0~0_combout\ $ (!\saida_comparador2~combout\ $ (!\saida_comparador1~combout\))) ) + ( \Add3~12_combout\ ) + ( \Add3~15_cout\ ))
-- \Add3~2\ = CARRY(( !\saida_comparador7~combout\ $ (!\Add0~0_combout\ $ (!\saida_comparador2~combout\ $ (!\saida_comparador1~combout\))) ) + ( \Add3~12_combout\ ) + ( \Add3~15_cout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111110000000000000000000000000110100110010110",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_saida_comparador7~combout\,
	datab => \ALT_INV_Add0~0_combout\,
	datac => \ALT_INV_saida_comparador2~combout\,
	datad => \ALT_INV_saida_comparador1~combout\,
	dataf => \ALT_INV_Add3~12_combout\,
	cin => \Add3~15_cout\,
	sumout => \Add3~1_sumout\,
	cout => \Add3~2\);

-- Location: LABCELL_X56_Y2_N42
\Add1~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add1~0_combout\ = ( \reg_switches[8]~input_o\ & ( \reg_switches[7]~input_o\ & ( (!\saida_rom[8]~input_o\ & (\saida_rom[9]~input_o\ & (\saida_rom[7]~input_o\ & \reg_switches[9]~input_o\))) # (\saida_rom[8]~input_o\ & (((\saida_rom[9]~input_o\ & 
-- \reg_switches[9]~input_o\)) # (\saida_rom[7]~input_o\))) ) ) ) # ( !\reg_switches[8]~input_o\ & ( \reg_switches[7]~input_o\ & ( (\saida_rom[9]~input_o\ & (\saida_rom[7]~input_o\ & \reg_switches[9]~input_o\)) ) ) ) # ( \reg_switches[8]~input_o\ & ( 
-- !\reg_switches[7]~input_o\ & ( (\saida_rom[8]~input_o\ & (\saida_rom[9]~input_o\ & \reg_switches[9]~input_o\)) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000001000100000000000000110000010100010111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_saida_rom[8]~input_o\,
	datab => \ALT_INV_saida_rom[9]~input_o\,
	datac => \ALT_INV_saida_rom[7]~input_o\,
	datad => \ALT_INV_reg_switches[9]~input_o\,
	datae => \ALT_INV_reg_switches[8]~input_o\,
	dataf => \ALT_INV_reg_switches[7]~input_o\,
	combout => \Add1~0_combout\);

-- Location: LABCELL_X56_Y2_N18
\Add7~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add7~0_combout\ = ( \saida_comparador2~combout\ & ( !\Add1~0_combout\ $ (((!\saida_comparador1~combout\ & (!\saida_comparador7~combout\ $ (\Add0~0_combout\))))) ) ) # ( !\saida_comparador2~combout\ & ( !\Add1~0_combout\ $ (((!\saida_comparador1~combout\) 
-- # (!\saida_comparador7~combout\ $ (\Add0~0_combout\)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000011011111001000001101111100101101111100100000110111110010000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_saida_comparador7~combout\,
	datab => \ALT_INV_Add0~0_combout\,
	datac => \ALT_INV_saida_comparador1~combout\,
	datad => \ALT_INV_Add1~0_combout\,
	dataf => \ALT_INV_saida_comparador2~combout\,
	combout => \Add7~0_combout\);

-- Location: LABCELL_X56_Y2_N36
\Add3~17\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add3~17_combout\ = ( \saida_comparador6~combout\ & ( \saida_comparador5~combout\ & ( (!\saida_rom[3]~input_o\) # ((!\saida_rom[4]~input_o\) # ((!\reg_switches[3]~input_o\) # (!\reg_switches[4]~input_o\))) ) ) ) # ( !\saida_comparador6~combout\ & ( 
-- \saida_comparador5~combout\ & ( (!\saida_rom[3]~input_o\ & (\saida_rom[4]~input_o\ & ((\reg_switches[4]~input_o\)))) # (\saida_rom[3]~input_o\ & (((\saida_rom[4]~input_o\ & \reg_switches[4]~input_o\)) # (\reg_switches[3]~input_o\))) ) ) ) # ( 
-- \saida_comparador6~combout\ & ( !\saida_comparador5~combout\ & ( (!\saida_rom[3]~input_o\ & (\saida_rom[4]~input_o\ & ((\reg_switches[4]~input_o\)))) # (\saida_rom[3]~input_o\ & (((\saida_rom[4]~input_o\ & \reg_switches[4]~input_o\)) # 
-- (\reg_switches[3]~input_o\))) ) ) ) # ( !\saida_comparador6~combout\ & ( !\saida_comparador5~combout\ & ( (\saida_rom[3]~input_o\ & (\saida_rom[4]~input_o\ & (\reg_switches[3]~input_o\ & \reg_switches[4]~input_o\))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000001000001010011011100000101001101111111111111111110",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_saida_rom[3]~input_o\,
	datab => \ALT_INV_saida_rom[4]~input_o\,
	datac => \ALT_INV_reg_switches[3]~input_o\,
	datad => \ALT_INV_reg_switches[4]~input_o\,
	datae => \ALT_INV_saida_comparador6~combout\,
	dataf => \ALT_INV_saida_comparador5~combout\,
	combout => \Add3~17_combout\);

-- Location: LABCELL_X56_Y2_N6
\Add3~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add3~5_sumout\ = SUM(( \Add3~17_combout\ ) + ( \Add7~0_combout\ ) + ( \Add3~2\ ))
-- \Add3~6\ = CARRY(( \Add3~17_combout\ ) + ( \Add7~0_combout\ ) + ( \Add3~2\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111100001111000000000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \ALT_INV_Add7~0_combout\,
	datad => \ALT_INV_Add3~17_combout\,
	cin => \Add3~2\,
	sumout => \Add3~5_sumout\,
	cout => \Add3~6\);

-- Location: LABCELL_X56_Y2_N54
\Add3~18\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add3~18_combout\ = ( \saida_comparador6~combout\ & ( \saida_comparador5~combout\ & ( (\saida_rom[3]~input_o\ & (\saida_rom[4]~input_o\ & (\reg_switches[3]~input_o\ & \reg_switches[4]~input_o\))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000000000000001",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_saida_rom[3]~input_o\,
	datab => \ALT_INV_saida_rom[4]~input_o\,
	datac => \ALT_INV_reg_switches[3]~input_o\,
	datad => \ALT_INV_reg_switches[4]~input_o\,
	datae => \ALT_INV_saida_comparador6~combout\,
	dataf => \ALT_INV_saida_comparador5~combout\,
	combout => \Add3~18_combout\);

-- Location: LABCELL_X56_Y2_N21
\Add7~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add7~1_combout\ = ( \saida_comparador2~combout\ & ( (\Add1~0_combout\ & ((!\saida_comparador7~combout\ $ (!\Add0~0_combout\)) # (\saida_comparador1~combout\))) ) ) # ( !\saida_comparador2~combout\ & ( (\Add1~0_combout\ & (\saida_comparador1~combout\ & 
-- (!\saida_comparador7~combout\ $ (!\Add0~0_combout\)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000110000000000000011000000110000011110000011000001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_saida_comparador7~combout\,
	datab => \ALT_INV_Add0~0_combout\,
	datac => \ALT_INV_Add1~0_combout\,
	datad => \ALT_INV_saida_comparador1~combout\,
	dataf => \ALT_INV_saida_comparador2~combout\,
	combout => \Add7~1_combout\);

-- Location: LABCELL_X56_Y2_N9
\Add3~9\ : cyclonev_lcell_comb
-- Equation(s):
-- \Add3~9_sumout\ = SUM(( \Add7~1_combout\ ) + ( \Add3~18_combout\ ) + ( \Add3~6\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000101010101010101000000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_Add3~18_combout\,
	datad => \ALT_INV_Add7~1_combout\,
	cin => \Add3~6\,
	sumout => \Add3~9_sumout\);

-- Location: LABCELL_X45_Y46_N3
\~QUARTUS_CREATED_GND~I\ : cyclonev_lcell_comb
-- Equation(s):

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
;
END structure;


